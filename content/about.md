+++
title = 'Sobre São Paulo'
date = 2023-10-03T16:17:50-03:00
draft = false
+++

Andar pelos museus paulistas é dar um verdadeiro passeio pela história do Brasil, de São Paulo e do mundo. São grandes monumentos, instalados em prédios modernos de arquitetura arrojada ou em áreas históricas delicadamente preservadas. Uma viagem pela Colônia, Império e República. O Velho e o Novo. Portinari, Tarsila do Amaral, Rodin, Miró, Brecheret, Di Cavalcanti. Bibliotecas, Espaços Culturais, Documentos, Manuscritos, Móveis, Roupas, Fotos, Vídeos, Música, Cinema e Artes Gráficas.